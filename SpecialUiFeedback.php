<?php
class SpecialUiFeedback extends SpecialPage {

    function __construct() {
        parent::__construct( 'UiFeedback' );
    }

    function execute( $par ) {
        $request = $this->getRequest();
        $output  = $this->getOutput();
        $this->setHeaders();

        $user = $this->getContext()->getUser();

        /* Rights to read and write */
        $can_read  = $user->isAllowed( 'read_uifeedback' );
        $can_write = $user->isAllowed( 'write_uifeedback' );

        $output_text = '';

        if ( !$can_read ) {
            $output_text = $this->msg( 'ui-feedback-special-no-permission' )->text();
        } else { /* can read */
            /* Arrays for Output */
            $importance_array = array( '', '--', '-', '0', '+', '++' );
            $happened_array   = array( '', $this->msg( 'ui-feedback-special-happened-1' )->text(), $this->msg( 'ui-feedback-special-happened-2' )->text(), $this->msg( 'ui-feedback-special-happened-3' )->text(), $this->msg( 'ui-feedback-special-happened-4' )->text() );
            $bool_array       = array( $this->msg( 'ui-feedback-special-no' )->text(), $this->msg( 'ui-feedback-special-yes' )->text() );
            $status_array     = array( $this->msg( 'ui-feedback-special-status-open' )->text(), $this->msg( 'ui-feedback-special-status-in-review' )->text(), $this->msg( 'ui-feedback-special-status-closed' )->text(), $this->msg( 'ui-feedback-special-status-declined' )->text() );


            /* get Request-data */
            $id = $request->getInt( 'id', -1 );

            $filter_status     = $request->getVal( 'filter_status', '0,1' ); // 0: open, 1: in review, 2: closed, 3: declined
            $filter_status     = array_map( 'intval', explode( ',', $filter_status ) );
            $filter_type       = $request->getVal( 'filter_type', '0,1' ); // 0: Questionnaire, 1: Screenshot
            $filter_type       = array_map( 'intval', explode( ',', $filter_type ) );
            $filter_importance = $request->getVal( 'filter_importance', '0,1,2,3,4,5' ); // 0: no, 1: -2, 2: -1, 3: 0, 4: 1, 5: 2
            $filter_importance = array_map( 'intval', explode( ',', $filter_importance ) );

            $order         = ' created DESC';
            $only_one_item = false;
            if ( $id >= 0 ) {
                $only_one_item = true;
                $order         = 'created ASC';
            }

            $conditions = array();

            /* connect to the DB*/
            $dbr = wfGetDB( DB_SLAVE );
            /* get the rows from uifeedback-table */
            if ( $id !== -1 ) {
                $conditions[ 'id' ] = $id;
            } else {
                /* if no checkbox is selected filter for -1 (which will not be found -> empty result) */
                if ( count( $filter_status ) ) {
                    $conditions[ 'status' ] = $filter_status;
                } else {
                    $conditions[ 'status' ] = '-1';
                }

                if ( count( $filter_status ) ) {
                    $conditions[ 'type' ] = $filter_type;
                } else {
                    $conditions[ 'type' ] = '-1';
                }

                if ( count( $filter_status ) ) {
                    $conditions[ 'importance' ] = $filter_importance;
                } else {
                    $conditions[ 'importance' ] = '-1';
                }

            }
            $res = $dbr->select(
                array( 'uifeedback' ),
                array(
                    'id',
                    'url',
                    'type',
                    'created',
                    'task',
                    'done', // Have you been able to carry out your intended task successfully?
                    'text1', // some more details
                    'importance', //
                    'happened',
                    'username',
                    'useragent',
                    'notify',
                    'CONCAT(uifeedback.id,CONCAT(\'#\',uifeedback.image_size)) as image_id',
                    'status',
                    'comment'
                ),
                $conditions,
                __METHOD__,
                array( 'ORDER BY' => $order )
            );
            /* number of rows selected */
            $count = $res->numRows();

            /* add table with filters */
            if ( !$only_one_item ) {
                /* Filter */
                $output_text .= '<div class="filters">';
                $output_text .= '<h2>' . $this->msg( 'ui-feedback-special-filter' )->text() . '</h2>';
                $output_text .= '<form action="' . SpecialPage::getTitleFor( 'UiFeedback' )->getFullURL() . '" method="GET">';
                $output_text .= '<table style="border-collapse: separate;border-spacing: 10px 5px;">';
                $output_text .= '<tr>';
                $output_text .= '<th>' . $this->msg( 'ui-feedback-special-table-head-status' )->text() . '</th>';
                $output_text .= '<th>' . $this->msg( 'ui-feedback-special-table-head-importance' )->text() . '</th>';
                $output_text .= '<th>' . $this->msg( 'ui-feedback-special-table-head-type' )->text() . '</th>';
                $output_text .= '</tr>';
                $output_text .= '<tr>';
                $output_text .= '<td><label><input type="checkbox" name="filter_status" value="0" ' . ( ( in_array( '0', $filter_status ) ) ? 'checked' : '' ) . '>' . $this->msg( 'ui-feedback-special-status-open' )->text() . '</label></td>';
                $output_text .= '<td><label><input type="checkbox" name="filter_importance" value="0" ' . ( ( in_array( '0', $filter_importance ) ) ? 'checked' : '' ) . '>' . $this->msg( 'ui-feedback-special-undefined' )->text() . '</label></td>';
                $output_text .= '<td><label><input type="checkbox" name="filter_type" value="1" ' . ( ( in_array( '1', $filter_type ) ) ? 'checked' : '' ) . '>' . $this->msg( 'ui-feedback-special-type-screenshot' )->text() . '</label></td>';
                $output_text .= '</tr>';
                $output_text .= '<tr>';
                $output_text .= '<td><label><input type="checkbox" name="filter_status" value="1" ' . ( ( in_array( '1', $filter_status ) ) ? 'checked' : '' ) . '>' . $this->msg( 'ui-feedback-special-status-in-review' )->text() . '</label></td>';
                $output_text .= '<td><label><input type="checkbox" name="filter_importance" value="1" ' . ( ( in_array( '1', $filter_importance ) ) ? 'checked' : '' ) . '>--</label></td>';
                $output_text .= '<td><label><input type="checkbox" name="filter_type" value="0" ' . ( ( in_array( '0', $filter_type ) ) ? 'checked' : '' ) . '>' . $this->msg( 'ui-feedback-special-type-questionnaire' )->text() . '</label></td>';
                $output_text .= '</tr>';
                $output_text .= '<tr>';
                $output_text .= '<td><label><input type="checkbox" name="filter_status" value="3" ' . ( ( in_array( '3', $filter_status ) ) ? 'checked' : '' ) . '>' . $this->msg( 'ui-feedback-special-status-declined' )->text() . '</label></td>';
                $output_text .= '<td><label><input type="checkbox" name="filter_importance" value="2" ' . ( ( in_array( '2', $filter_importance ) ) ? 'checked' : '' ) . '>-</label></td>';
                $output_text .= '<td>&nbsp;</td>';
                $output_text .= '</tr>';
                $output_text .= '<tr>';
                $output_text .= '<td><label><input type="checkbox" name="filter_status" value="2" ' . ( ( in_array( '2', $filter_status ) ) ? 'checked' : '' ) . '>' . $this->msg( 'ui-feedback-special-status-closed' )->text() . '</label></td>';
                $output_text .= '<td><label><input type="checkbox" name="filter_importance" value="3" ' . ( ( in_array( '3', $filter_importance ) ) ? 'checked' : '' ) . '>0</label></td>';
                $output_text .= '<td>&nbsp;</td>';
                $output_text .= '</tr>';
                $output_text .= '<tr>';
                $output_text .= '<td>&nbsp;</td>';
                $output_text .= '<td><label><input type="checkbox" name="filter_importance" value="4" ' . ( ( in_array( '4', $filter_importance ) ) ? 'checked' : '' ) . '>+</label></td>';
                $output_text .= '<td>&nbsp;</td>';
                $output_text .= '</tr>';
                $output_text .= '<tr>';
                $output_text .= '<td>&nbsp;</td>';
                $output_text .= '<td><label><input type="checkbox" name="filter_importance" value="5" ' . ( ( in_array( '5', $filter_importance ) ) ? 'checked' : '' ) . '>++</label></td>';
                $output_text .= '<td style="text-align:right;"><input type="button" value="filter" id="ui-feedback-set-filter-button"></td>';
                $output_text .= '</tr>';
                $output_text .= '</table>';
                $output_text .= '</form>';
                $output_text .= '</div>'; // end filters

                /* list of top5 contributers */
                $output_text .= '<div class="stats">';
                $output_text .= '<h2>' . $this->msg( 'ui-feedback-special-stats' )->text() . '</h2>';
                $output_text .= '<div style="float:left;">';
                /* get name and number of closed feedback-posts */
                $res_stats = $dbr->select(
                    array( 'uifeedback' ),
                    array( 'username', 'count(id) as count' ),
                    'username != \'\' and status = \'2\'', /* no anon users */
                    __METHOD__,
                    array( 'GROUP BY' => 'username', 'ORDER BY' => 'count DESC', 'LIMIT' => '5' )
                );
                $output_text .= $this->msg( 'ui-feedback-special-top5-users' )->text();
                $output_text .= '<table style="text-align:right;border-collapse: separate;border-spacing: 10px 5px;">';
                /* add rows to table */
                foreach ( $res_stats as $row ) {
                    $output_text .= Xml::tags( 'tr', null, Html::element( 'td', array(), $row->username ) . Html::element( 'td', array( 'style' => "text-align:right;" ), $row->count ) );
                }
                $output_text .= '</table>';
                $output_text .= '</div>'; /* end users */

                $output_text .= '</div>'; /* end stats */

            } else {
                /* add page-navigation */
                $output_text .= '<div class="page_navi">';
                /* previous */
                if ( $id > 1 ) {
                    $output_text .= Html::element( 'a', array( 'href' => SpecialPage::getTitleFor( 'UiFeedback' )->getFullURL( array( 'id' => ( $id - 1 ) ) ) ), $this->msg( 'ui-feedback-special-navi-previous' )->text() );
                    $output_text .= "&nbsp;|&nbsp;";
                }
                /* next */
                $output_text .= Html::element( 'a', array( 'href' => SpecialPage::getTitleFor( 'UiFeedback' )->getFullURL( array( 'id' => ( $id + 1 ) ) ) ), $this->msg( 'ui-feedback-special-navi-next' )->text() );
                $output_text .= "&nbsp;|&nbsp;";
                /* all */
                $output_text .= Html::element( 'a', array( 'href' => SpecialPage::getTitleFor( 'UiFeedback' )->getFullURL( array( 'id' => ( -1 ) ) ) ), $this->msg( 'ui-feedback-special-navi-all' )->text() );

                $output_text .= '</div>';
                /* end page-navi */
            }

            /* create the table */
            if ( $count > 0 ) {
                /* Browser and Operating-System-Icons */
                $internetexplorer_icon = '<div class="icon ie">1</div>';
                $firefox_icon          = '<div class="icon ff">2</div>';
                $chrome_icon           = '<div class="icon ch">3</div>';
                $safari_icon           = '<div class="icon sf">4</div>';
                $opera_icon            = '<div class="icon op">5</div>';
                $window_icon           = '<div class="icon win">1</div>';
                $mac_icon              = '<div class="icon mac">2</div>';
                $linux_icon            = '<div class="icon lin">3</div>';

                $output_text .= Html::element( 'h2', array( 'style' => 'clear:both;' ), $this->msg( 'ui-feedback-special-feedback' )->text() );
                /* if more than one item is visible add "found 37 items:" */
                if ( !$only_one_item )
                    $output_text .= $this->msg( 'ui-feedback-special-found' )->text() . ' ' . $count . ' ' . $this->msg( 'ui-feedback-special-items' )->text() . ':';

                /* Result-Table */
                $output_text .= '<table class="wikitable sortable jquery-tablesorter">';
                $output_text .= '<tr>';
                /* Headlines */
                /* id */
                $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort' ), $this->msg( 'ui-feedback-special-table-head-id' )->text() );
                /* username */
                $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort' ), $this->msg( 'ui-feedback-special-table-head-username' )->text() );
                /* browser */
                $output_text .= '<th scope="col" class="headerSort"></th>';
                /* OS */
                if ( $can_write )
                    $output_text .= '<th scope="col" class="headerSort"></th>';
                /* time */
                if ( $only_one_item ) {
                    $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort' ), $this->msg( 'ui-feedback-special-table-head-time' )->text() );
                }
                /* type */
                $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort' ), $this->msg( 'ui-feedback-special-table-head-type' )->text() );
                /* importance */
                $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort' ), $this->msg( 'ui-feedback-special-table-head-importance' )->text() );
                /* happened */
                $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort' ), $this->msg( 'ui-feedback-special-table-head-happened' )->text() );
                /* task */
                $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort' ), $this->msg( 'ui-feedback-special-table-head-task' )->text() );
                /* done */
                $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort' ), $this->msg( 'ui-feedback-special-table-head-done' )->text() );
                if ( !$only_one_item ) { // Dont display the freetext-lines in one-entry-view
                    /* text1 */
                    $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort' ), $this->msg( 'ui-feedback-special-table-head-details' )->text() );
                }
                /* status */
                $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort' ), $this->msg( 'ui-feedback-special-table-head-status' )->text() );
                /* comment */
                $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort' ), $this->msg( 'ui-feedback-special-table-head-notes' )->text() );
                /* Notify */
                if ( $can_write ) {
                    $output_text .= Html::element( 'th', array( 'scope' => 'col', 'class' => 'headerSort', 'title' => $this->msg( 'ui-feedback-special-tooltip-notify' )->text() ) );
                }
                /* end Row*/
                $output_text .= '</tr>';

                /* Rows */
                foreach ( $res as $row ) {
                    $output_text .= '<tr>';
                    /* id */
                    $output_text .= Xml::tags( 'td', null, Html::element( 'a', array( 'href' => SpecialPage::getTitleFor( 'UiFeedback' )->getFullURL( array( 'id' => $row->id ) ) ), $row->id ) );
                    /* username */
                    if ( $row->username === '' ) {
                        $output_text .= Html::element( 'td', null, $this->msg( 'ui-feedback-special-anonymous' )->text() );
                    } else {
                        $output_text .= Xml::tags( 'td', null, Html::element( 'a', array( 'href' => Title::makeTitleSafe( NS_USER_TALK, $row->username )->getFullURL() ), $row->username ) );
                    }
                    /* browser */
                    /* using Html::element here seems to be more confusing then having it this way */
                    if ( $can_write ) {
                        $output_text .= '<td title="' . htmlspecialchars( $row->useragent ) . '">';
                    } else {
                        $output_text .= '<td>';
                    }
                    if ( strpos( '#' . $row->useragent, 'Chrome' ) ) {
                        $output_text .= $chrome_icon;
                    } else if ( strpos( '#' . $row->useragent, 'Safari' ) ) {
                        $output_text .= $safari_icon;
                    } else if ( strpos( '#' . $row->useragent, 'Firefox' ) ) {
                        $output_text .= $firefox_icon;
                    } else if ( strpos( '#' . $row->useragent, 'MSIE' ) ) {
                        $output_text .= $internetexplorer_icon;
                    } else if ( strpos( '#' . $row->useragent, 'Opera' ) ) {
                        $output_text .= $opera_icon;
                    } else {
                        $output_text .= '?';
                    }
                    $output_text .= '</td>';
                    /* os - only visible to admins */
                    if ( $can_write ) {
                        $output_text .= '<td title="' . htmlspecialchars( $row->useragent ) . '">';
                        if ( strpos( '#' . $row->useragent, 'Windows' ) )
                            $output_text .= $window_icon;
                        else if ( strpos( '#' . $row->useragent, 'Mac OS' ) )
                            $output_text .= $mac_icon;
                        else if ( strpos( '#' . $row->useragent, 'Linux' ) )
                            $output_text .= $linux_icon;
                        else
                            $output_text .= '<div>?</div>';
                        $output_text .= '</td>';
                    }
                    /* time */
                    if ( $only_one_item ) {
                        $output_text .= Html::element( 'td', null, $row->created );
                    }
                    /* type */
                    if ( $row->type === '1' ) {
                        $output_text .= Xml::tags( 'td', null, Xml::tags( 'a', array( 'href' => SpecialPage::getTitleFor( 'UiFeedback' )->getFullURL( array( 'id' => htmlspecialchars( $row->id ) ) ) ), Html::element( 'div', array( 'class' => 'icon screenshot-icon', 'title' => $this->msg( 'ui-feedback-special-type-screenshot' )->text() ), 1 ) ) );
                    } else {
                        $output_text .= Xml::tags( 'td', null, Xml::tags( 'a', array( 'href' => SpecialPage::getTitleFor( 'UiFeedback' )->getFullURL( array( 'id' => htmlspecialchars( $row->id ) ) ) ), Html::element( 'div', array( 'class' => 'icon questionnaire-icon', 'title' => $this->msg( 'ui-feedback-special-type-questionnaire' )->text() ), 2 ) ) );
                    }
                    /* importance */
                    if ( $row->importance == 0 ) {
                        $output_text .= '<td>&nbsp;</td>';
                    } else {
                        $output_text .= '<td>' . $importance_array[ $row->importance ] . '</td>';
                    }
                    /* happened */
                    $output_text .= '<td>' . $happened_array[ $row->happened ] . '</td>';
                    /* task */
                    $output_text .= Html::element( 'td', null, $row->task );
                    /* done */
                    if ( is_null( $row->done ) )
                        $output_text .= '<td></td>';
                    else
                        $output_text .= '<td>' . $bool_array[ $row->done ] . '</td>';
                    if ( !$only_one_item ) { // dont display the freetext-fields in the one-entry-only-view
                        /* text1 */
                        if ( strlen( $row->text1 ) > 50 ) {
                            $output_text .= Html::element( 'td', null, $this->getContext()->getLanguage()->truncate( $row->text1, 50, $this->msg( 'ellipsis' )->text() ) );
                        } else {
                            $output_text .= Html::element( 'td', null, $row->text1 );
                        }
                    }
                    /* status */
                    $output_text .= '<td>';
                    if ( $row->status == 0 ) {
                        $output_text .= Html::element( 'b', null, $this->msg( 'ui-feedback-special-status-open' )->text() );
                        $output_text .= '<br/>';
                        /* only admins can change the status */
                        if ( $can_write ) {
                            $output_text .= Html::element( 'a', array( 'href' => SpecialPage::getTitleFor( 'UiFeedback' )->getFullURL( array( 'id' => htmlspecialchars( $row->id ) ) ) ), $this->msg( 'ui-feedback-special-status-in-review' )->text() );
                        }
                    } else if ( $row->status == 1 ) {
                        /* only admins can change the status*/
                        if ( $can_write ) {
                            $output_text .= Xml::tags( 'a', array( 'href' => SpecialPage::getTitleFor( 'UiFeedback' )->getFullURL( array( 'id' => htmlspecialchars( $row->id ) ) ) ), Html::element( 'b', null, $this->msg( 'ui-feedback-special-status-in-review' )->text() ) );
                        } else {
                            $output_text .= Html::element( 'b', null, $this->msg( 'ui-feedback-special-status-in-review' )->text() );
                        }
                    } else if ( $row->status == 2 ) {
                        $output_text .= htmlspecialchars( $this->msg( 'ui-feedback-special-status-closed' )->text() ) . '<br/>';
                    } else if ( $row->status == 3 ) {
                        $output_text .= htmlspecialchars( $this->msg( 'ui-feedback-special-status-declined' )->text() ) . '<br/>';
                    }
                    $output_text .= '</td>';
                    /* comment */
                    $output_text .= Html::element( 'td', null, $row->comment );
                    /* notify - only admins see this */
                    if ( $can_write ) {
                        if ( $row->notify ) {
                            $output_text .= Xml::tags( 'td', array( 'title' => $this->msg( 'ui-feedback-special-tooltip-notify' )->text() ), Xml::tags( 'a', array( 'href' => Title::makeTitleSafe( NS_USER_TALK, $row->username )->getFullURL() ), Html::element( 'div', array( 'class' => 'icon notify' ), 1 ) ) );
                        } else {
                            $output_text .= '<td></td>';
                        }
                    }
                    /* end row */
                    $output_text .= '</tr>';
                }
                $output_text .= '</table>';
                /* end create table */

                /* statistics about presenting the different request-methods and how often they have been clicked */
                /* this information is not usefull for 'normal' users, so only admins will see it */
                if ( !$only_one_item && $can_write ) {
                    $output_text .= '<div style="border:1px solid black;background:#FCFCFC;padding:5px;width:325px">';
                    $type_array = array( $this->msg( 'ui-feedback-special-stats-type-1' )->text(),
                        $this->msg( 'ui-feedback-special-stats-type-2' )->text(),
                        $this->msg( 'ui-feedback-special-stats-type-3' )->text()
                    );
                    /*get rows from database*/
                    $res_stats = $dbr->select( array( 'uifeedback_stats' ), array( 'type', 'shown', 'clicked', 'sent' ), '', __METHOD__, array( 'ORDER BY' => 'type DESC' ) );
                    $output_text .= htmlspecialchars( $this->msg( 'ui-feedback-special-stats-head' )->text() );
                    $output_text .= '<table style="text-align:right;border-collapse: separate;border-spacing: 10px 5px;">';
                    $output_text .= '<tr><th>' . $this->msg( 'ui-feedback-special-stats-type' )->text() . '</th><th>' . $this->msg( 'ui-feedback-special-stats-shown' )->text() . '</th><th>' . $this->msg( 'ui-feedback-special-stats-clicked' )->text() . '</th><th>' . $this->msg( 'ui-feedback-special-stats-sent' )->text() . '</th></tr>';
                    /* add rows to the table */
                    foreach ( $res_stats as $row_stats ) {
                        $output_text .= '<tr>';
                        $output_text .= Html::element( 'td', array( 'style' => 'text-align:right;' ), $type_array[ $row_stats->type ] );
                        $output_text .= Html::element( 'td', array( 'style' => 'text-align:right;' ), $row_stats->shown );
                        $output_text .= Html::element( 'td', array( 'style' => 'text-align:right;' ), $row_stats->clicked );
                        $output_text .= Html::element( 'td', array( 'style' => 'text-align:right;' ), $row_stats->sent );
                        $output_text .= '</tr>';
                    }
                    $output_text .= '</table>';
                    $output_text .= '</div>';
                } /* end show and click stats */

            } else {
                $output_text .= '<h2 style="clear:both;">' . $this->msg( 'ui-feedback-special-feedback' )->text() . '</h2>' . $this->msg( 'ui-feedback-special-nothing-found' )->text();
            }

            /* One-Feedback-Item-View */
            if ( $only_one_item ) {
                $output_text .= '<h2>URL</h2>';
                $output_text .= Html::element( 'a', array( 'href' => $row->url ), $row->url );

                if ( $row->type === '1' ) { /* screenshot Feedback */
                    $output_text .= Html::element( 'h2', null, $this->msg( 'ui-feedback-special-table-head-details' )->text() );
                    $output_text .= htmlspecialchars( $row->text1 );
                    if ( strlen( $row->text1 ) == 0 ) {
                        $output_text .= Html::element( 'i', null, $this->msg( 'ui-feedback-special-table-head-none' )->text() );
                    }
                } else { /* Questionnaire Feedback */
                    $output_text .= Html::element( 'h2', null, $this->msg( 'ui-feedback-special-table-head-details' )->text() );
                    $output_text .= htmlspecialchars( $row->text1 );
                    if ( strlen( $row->text1 ) == 0 ) {
                        $output_text .= Html::element( 'i', null, $this->msg( 'ui-feedback-special-table-head-none' )->text() );
                    }
                }
                $output_text .= '<div>';

                /* Review Form - only for admins */
                if ( $can_write ) {
                    $output_text .= '<div style = "float:left;">';
                    $output_text .= '<h1>' . htmlspecialchars( $this->msg( 'ui-feedback-special-review' )->text() ) . '</h1>';
                    if ( $row->notify )
                        $output_text .= '' . htmlspecialchars( $this->msg( 'ui-feedback-special-info' )->text() ) . ': <i>' . htmlspecialchars( $this->msg( 'ui-feedback-special-tooltip-notify' )->text() ) . '</i><br/>';
                    $output_text .= '<form name="review" method="post" id="ui-review-form" action="">';
                    $output_text .= '<div style="float:left;">';
                    $output_text .= '' . htmlspecialchars( $this->msg( 'ui-feedback-special-table-head-status' )->text() ) . ':<br/>';
                    $output_text .= '<label><input type="radio" name="status" value="1" ' . ( ( $row->status == 1 ) ? 'checked' : '' ) . '>' . htmlspecialchars( $this->msg( 'ui-feedback-special-status-in-review' )->text() ) . '</label><br/>';
                    $output_text .= '<label><input type="radio" name="status" value="2" ' . ( ( $row->status == 2 ) ? 'checked' : '' ) . '>' . htmlspecialchars( $this->msg( 'ui-feedback-special-status-closed' )->text() ) . '</label><br/>';
                    $output_text .= '<label><input type="radio" name="status" value="3" ' . ( ( $row->status == 3 ) ? 'checked' : '' ) . '>' . htmlspecialchars( $this->msg( 'ui-feedback-special-status-declined' )->text() ) . '</label><br/>';
                    $output_text .= '</div>';
                    $output_text .= '<div style="float:left;margin-left:20px">';
                    $output_text .= '<label>' . htmlspecialchars( $this->msg( 'ui-feedback-special-table-head-notes' )->text() ) . ':<br/><textarea name="comment" rows="5" style="width:300px"></textarea></label>';
                    $output_text .= '<input type="hidden" name="id" value="' . $id . '">';
                    $output_text .= '<input type="hidden" name="method" value="review">';
                    $output_text .= '<br/><input type="button" value="send" id="ui-feedback-send-review-button" >';
                    $output_text .= '</div></form>';
                    $output_text .= '</div>';
                }
                /* previous Comments/Reviews */
                $res = $dbr->select(
                    array( 'uifeedback_reviews' ),
                    array( 'created', 'reviewer', 'status', 'comment' ),
                    array( 'feedback_id' => $id ),
                    __METHOD__,
                    array( 'ORDER BY' => 'created DESC' )
                );
                if ( $res->numRows() > 0 ) {
                    $output_text .= '<div style = "clear:both;">';
                    $output_text .= '<h1>' . htmlspecialchars( $this->msg( 'ui-feedback-special-previous-notes' )->text() ) . '</h1>';
                    $output_text .= '<ul>';
                    foreach ( $res as $review_row ) {

                        $output_text .= '<li>' . htmlspecialchars( $review_row->created ) . ' - ' . htmlspecialchars( $review_row->reviewer ) . ' - <b>' . $status_array[ $review_row->status ] . '</b>:<br/>' . htmlspecialchars( $review_row->comment ) . '</li>';
                    }
                    $output_text .= '</ul>';
                    $output_text .= '</div>';

                    $output_text .= '</div>';
                }
                /* end previous comments */

                /* Screenshot */
                if ( $row->type == '1' ) {
                    $output_text .= '<div style="clear: both;">';
                    $output_text .= '<h2>' . $this->msg( 'ui-feedback-special-type-screenshot' )->text() . ':</h2>';

                    $output_text .= Xml::tags( 'a', array( 'href' => wfFindFile( 'UIFeedback_screenshot_' . $row->id . '.png' )->getFullUrl() ), Html::element('img', array('alt'=>'screenshot', 'src' => wfFindFile( 'UIFeedback_screenshot_' . $row->id . '.png' )->createThumb( 600, 600 ))) );
//                    $output_text .= '<img style="max-width:800px;cursor:pointer;" src="' . wfFindFile( 'UIFeedback_screenshot_' . $row->id . '.png' )->createThumb( 600, 600 ) . '" alt="screenshot" onclick="$(this).css(\'max-width\',\'\').css(\'cursor\',\'auto\');">';

//                    $output_text .= '<img style="max-width:800px;cursor:pointer;" src="' . SpecialPage::getTitleFor( 'UiFeedback_api' )->getFullURL( array( 'getScreenshotByID' => htmlspecialchars( $row->id ) ) ) . '" alt="screenshot" onclick="$(this).css(\'max-width\',\'\').css(\'cursor\',\'auto\');">';
                    $output_text .= '</div>';
                }
            }
        }

        /* write to output */
        $output->addHTML( $output_text );
    }


}
