<?php
/**
 * Internationalisation for UiFeedback extension
 *
 * @file
 * @ingroup Extensions
 */

$messages =
    array();

/** English
 *
 * @author lbenedix
 */
$messages[ 'en' ] = array(
    'uifeedback'                                => 'UI-Feedback', // Ignore

    'ui-feedback-desc'                          => 'This extension allows Users to give feedback about the user interface',

    'ui-feedback-headline'                      => 'Feedback',
    'ui-feedback-scr-headline'                  => 'Feedback',

    'ui-feedback-task-label'                    => 'I wanted to:',
    'ui-feedback-task-1'                        => 'add/edit an item', // item
    'ui-feedback-task-2'                        => 'add/edit a label', // label
    'ui-feedback-task-3'                        => 'add/edit a description', // description
    'ui-feedback-task-4'                        => 'add/edit an alias', // alias
    'ui-feedback-task-5'                        => 'add/edit links', // links
    'ui-feedback-task-6'                        => 'search', // search
    'ui-feedback-task-7'                        => 'other:', // other

    'ui-feedback-done-label'                    => 'Were you able to complete your task?',

    'ui-feedback-good-label'                    => 'What happened while you were editing?',
    'ui-feedback-bad-label'                     => 'What behavior have you been expecting?',
    'ui-feedback-comment-label'                 => 'Please give us some more details:',

    'ui-feedback-happened-label'                => 'Tell us what happened:',
    'ui-feedback-happened-1'                    => 'Something did not work as expected',
    'ui-feedback-happened-2'                    => 'I got confused',
    'ui-feedback-happened-3'                    => 'I am missing a feature',
    'ui-feedback-happened-4'                    => 'Something else',


    'ui-feedback-importance-label'              => 'How important is it for you:',
    'ui-feedback-importance-1'                  => 'not important',
    'ui-feedback-importance-5'                  => 'very important',

    'ui-feedback-anonym-label'                  => 'I want to post it privatly.',
    'ui-feedback-anonym-help'                   => 'Check this box if you don\'t want to share your Username. Please note that we will not be able to keep you updated on your submitted issue if you choose to remain anonymous.',
    'ui-feedback-notify-label'                  => 'I want to be updated on this issue.',
    'ui-feedback-notify-help'                   => 'We will leave you a note on your userpage when we work on your issue.',


    'ui-feedback-yes'                           => 'yes',
    'ui-feedback-no'                            => 'no',

    'ui-feedback-problem-reset'                 => 'reset',
    'ui-feedback-problem-send'                  => 'send',
    'ui-feedback-problem-close'                 => 'close',
    'ui-feedback-problem-cancel'                => 'cancel',

    'ui-feedback-highlight-label'               => 'Click and drag an area to help us, to understand your feedback',

    'ui-feedback-yellow'                        => 'Highlight areas that are relevant.',
    'ui-feedback-black'                         => 'Blackout any personal information.',

    'ui-feedback-help-headline'                 => 'What to report:',
    'ui-feedback-help-subheading'               =>'How to Use:',
    'ui-feedback-help-text-top'                 => 'Elements and interactions that: <ul><li>prevent task completion</li><li>have an effect on task performance or cause a significant delay</li><li>make suggestion necessary</li><li>confuse and frustrate you</li><li>is a minor but annoying detail</li></ul>',
    'ui-feedback-help-text-bottom'              => 'You can highlight and blackout areas on the page to give us a hint where to look at',

    'ui-feedback-prerender-headline'            => 'Confirm Screenshot-Feedback',
    'ui-feedback-prerender-text1'               => 'A screenshot will now be rendered and uploaded to the server. That could take some time.',
    'ui-feedback-prerender-text2'               => 'Please don\'t close the browser until you see the message "Feedback sent".',


    /*Specialpage texts*/
    'ui-feedback-special-feedback'              => 'Feedback',
    'ui-feedback-special-stats'                 => 'Stats',

    'ui-feedback-special-nothing-found'         => 'Nothing found',
    'ui-feedback-special-found'                 => 'found',
    'ui-feedback-special-items'                 => 'items',
    'ui-feedback-special-none'                  => 'none',


    'ui-feedback-special-no-permission'         => 'You have not the right permissions to see that Page',

    'ui-feedback-special-happened-1'            => 'did not work as expected',
    'ui-feedback-special-happened-2'            => 'got confused',
    'ui-feedback-special-happened-3'            => 'missing feature',
    'ui-feedback-special-happened-4'            => 'other',

    'ui-feedback-special-yes'                   => 'yes',
    'ui-feedback-special-no'                    => 'no',

    'ui-feedback-special-status-open'           => 'open',
    'ui-feedback-special-status-in-review'      => 'in review',
    'ui-feedback-special-status-closed'         => 'closed',
    'ui-feedback-special-status-declined'       => 'declined',

    'ui-feedback-special-filter'                => 'Filter',

    'ui-feedback-special-undefined'             => 'undefined',

    'ui-feedback-special-type-screenshot'       => 'Screenshot',
    'ui-feedback-special-type-questionnaire'    => 'Questionnaire',

    'ui-feedback-special-top5-users'            => 'Users with most submissions (by closed feedback):',

    'ui-feedback-special-navi-previous'         => 'previous',
    'ui-feedback-special-navi-next'             => 'next',
    'ui-feedback-special-navi-all'              => 'all',

    'ui-feedback-special-table-head-none'       => 'none',

    'ui-feedback-special-table-head-id'         => 'ID',
    'ui-feedback-special-table-head-username'   => 'Username',
    'ui-feedback-special-table-head-time'       => 'Timestamp',
    'ui-feedback-special-table-head-type'       => 'Type',
    'ui-feedback-special-table-head-importance' => 'Importance',
    'ui-feedback-special-table-head-happened'   => 'What happened',
    'ui-feedback-special-table-head-task'       => 'Task',
    'ui-feedback-special-table-head-done'       => 'Done',
    'ui-feedback-special-table-head-details'    => 'Details',
    'ui-feedback-special-table-head-status'     => 'Status',
    'ui-feedback-special-table-head-notes'      => 'Notes',

    'ui-feedback-special-tooltip-notify'        => 'This user wants to be notified about status changes',

    'ui-feedback-special-anonymous'             => 'anonymous',


    'ui-feedback-special-stats-head'            => 'Number of shown and clicked requests for feedback:',
    'ui-feedback-special-stats-type-1'          => 'pop-up after edit',
    'ui-feedback-special-stats-type-2'          => 'questionnaire button',
    'ui-feedback-special-stats-type-3'          => 'screenshot button',

    'ui-feedback-special-stats-type'            => 'type',
    'ui-feedback-special-stats-shown'           => 'shown',
    'ui-feedback-special-stats-clicked'         => 'clicked',
    'ui-feedback-special-stats-sent'            => 'sent',


    'ui-feedback-special-review'                => 'Review',
    'ui-feedback-special-previous-notes'        => 'Previous Notes',
    'ui-feedback-special-info'                  => 'Info',


);

$messages[ 'qqq' ] = array(
    'uifeedback'                                => 'UI-Feedback',

    'ui-feedback-desc'                          => 'This extension allows Users to give feedback about the user interface',

    'ui-feedback-headline'                      => 'Feedback',
    'ui-feedback-scr-headline'                  => 'Feedback',

    'ui-feedback-task-label'                    => 'I wanted to:',
    'ui-feedback-task-1'                        => 'add/edit an item', // item
    'ui-feedback-task-2'                        => 'add/edit a label', // label
    'ui-feedback-task-3'                        => 'add/edit a description', // description
    'ui-feedback-task-4'                        => 'add/edit an alias', // alias
    'ui-feedback-task-5'                        => 'add/edit links', // links
    'ui-feedback-task-6'                        => 'search', // search
    'ui-feedback-task-7'                        => 'other:', // other

    'ui-feedback-done-label'                    => 'Were you able to complete your task?',

    'ui-feedback-good-label'                    => 'What happened while you were editing?',
    'ui-feedback-bad-label'                     => 'What behavior have you been expecting?',
    'ui-feedback-comment-label'                 => 'Please give us some more details:',

    'ui-feedback-happened-label'                => 'Tell us what happened:',
    'ui-feedback-happened-1'                    => 'Something did not work as expected',
    'ui-feedback-happened-2'                    => 'I got confused',
    'ui-feedback-happened-3'                    => 'I am missing a feature',
    'ui-feedback-happened-4'                    => 'Something else',


    'ui-feedback-importance-label'              => 'How important is it for you:',
    'ui-feedback-importance-1'                  => 'not important',
    'ui-feedback-importance-5'                  => 'very important',

    'ui-feedback-anonym-label'                  => 'I want to post it privatly.',
    'ui-feedback-anonym-help'                   => 'Check this box if you don\'t want to share your Username. Please note that we will not be able to keep you updated on your submitted issue if you choose to remain anonymous.',
    'ui-feedback-notify-label'                  => 'I want to be updated on this issue.',
    'ui-feedback-notify-help'                   => 'We will leave you a note on your userpage when we work on your issue.',


    'ui-feedback-yes'                           => 'yes',
    'ui-feedback-no'                            => 'no',

    'ui-feedback-problem-reset'                 => 'reset',
    'ui-feedback-problem-send'                  => 'send',
    'ui-feedback-problem-close'                 => 'close',
    'ui-feedback-problem-cancel'                => 'cancel',

    'ui-feedback-highlight-label'               => 'Click and drag an area to help us, to understand your feedback',

    'ui-feedback-yellow'                        => 'Highlight areas that are relevant.',
    'ui-feedback-black'                         => 'Blackout any personal information.',

    'ui-feedback-help-text'                     => 'You can highlight and blackout areas on the page to give us a hint where to look at',

    'ui-feedback-prerender-text1'               => 'A screenshot will now be rendered and uploaded to the server. That could take some time.',
    'ui-feedback-prerender-text2'               => 'Please don\'t close the browser until you see the message "Feedback sent".',


    /*Specialpage texts*/
    'ui-feedback-special-feedback'              => 'Feedback',
    'ui-feedback-special-stats'                 => 'Stats',

    'ui-feedback-special-nothing-found'         => 'Nothing found',
    'ui-feedback-special-found'                 => 'found',
    'ui-feedback-special-items'                 => 'items',
    'ui-feedback-special-none'                  => 'none',


    'ui-feedback-special-no-permission'         => 'You have not the right permissions to see that Page',

    'ui-feedback-special-happened-1'            => 'did not work as expected',
    'ui-feedback-special-happened-2'            => 'got confused',
    'ui-feedback-special-happened-3'            => 'missing feature',
    'ui-feedback-special-happened-4'            => 'other',

    'ui-feedback-special-yes'                   => 'yes',
    'ui-feedback-special-no'                    => 'no',

    'ui-feedback-special-status-open'           => 'open',
    'ui-feedback-special-status-in-review'      => 'in review',
    'ui-feedback-special-status-closed'         => 'closed',
    'ui-feedback-special-status-declined'       => 'declined',

    'ui-feedback-special-filter'                => 'Filter',

    'ui-feedback-special-undefined'             => 'undefined',

    'ui-feedback-special-type-screenshot'       => 'Screenshot',
    'ui-feedback-special-type-questionnaire'    => 'Questionnaire',

    'ui-feedback-special-top5-users'            => 'Users with most submissions (by closed feedback):',

    'ui-feedback-special-navi-previous'         => 'previous',
    'ui-feedback-special-navi-next'             => 'next',
    'ui-feedback-special-navi-all'              => 'all',

    'ui-feedback-special-table-head-none'       => 'none',

    'ui-feedback-special-table-head-id'         => 'ID',
    'ui-feedback-special-table-head-username'   => 'Username',
    'ui-feedback-special-table-head-time'       => 'Timestamp',
    'ui-feedback-special-table-head-type'       => 'Type',
    'ui-feedback-special-table-head-importance' => 'Importance',
    'ui-feedback-special-table-head-happened'   => 'What happened',
    'ui-feedback-special-table-head-task'       => 'Task',
    'ui-feedback-special-table-head-done'       => 'Done',
    'ui-feedback-special-table-head-details'    => 'Details',
    'ui-feedback-special-table-head-status'     => 'Status',
    'ui-feedback-special-table-head-notes'      => 'Notes',

    'ui-feedback-special-tooltip-notify'        => 'This user wants to be notified about status changes',

    'ui-feedback-special-anonymous'             => 'anonymous',


    'ui-feedback-special-stats-head'            => 'Number of shown and clicked requests for feedback:',
    'ui-feedback-special-stats-type-1'          => 'pop-up after edit',
    'ui-feedback-special-stats-type-2'          => 'questionnaire button',
    'ui-feedback-special-stats-type-3'          => 'screenshot button',

    'ui-feedback-special-stats-type'            => 'type',
    'ui-feedback-special-stats-shown'           => 'shown',
    'ui-feedback-special-stats-clicked'         => 'clicked',
    'ui-feedback-special-stats-sent'            => 'sent',


    'ui-feedback-special-review'                => 'Review',
    'ui-feedback-special-previous-notes'        => 'Previous Notes',
    'ui-feedback-special-info'                  => 'Info',


);
